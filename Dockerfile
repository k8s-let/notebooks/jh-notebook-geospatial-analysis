FROM registry.ethz.ch/k8s-let/notebooks/jh-notebook-base-ethz:3.0.0-12

USER root

# RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install cmake==3.18.4

COPY start-singleuser.sh /usr/local/bin/

RUN mamba env create -f /builds/k8s-let/notebooks/jh-notebook-geospatial-analysis/environment.yml && \
  source activate cmga-env && \
  python -m ipykernel install --name=cmga-env --display-name='Geospatial Analysis'
  

USER 1000
